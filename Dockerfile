FROM docker.io/ruby:2.6-slim-buster

# Diaspora image for OpenShift Origin

ARG DO_UPGRADE=
ENV DIASPORA_VERS=0.7.15.0 \
    PATH=$PATH:/diaspora/bin \
    RAILS_ENV=production

LABEL io.k8s.description="Diaspora Image." \
      io.k8s.display-name="Diaspora" \
      io.openshift.expose-services="8080:diaspora" \
      io.openshift.tags="diaspora" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-diaspora" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$DIASPORA_VERS"

COPY config/* /

RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends dumb-init \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 /diaspora/log \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Diaspora Dependencies" \
    && apt-get install -y build-essential libssl-dev libcurl4-openssl-dev \
	libxml2-dev libxslt-dev imagemagick ghostscript curl git openssl \
	libmagickwand-dev libpq-dev nodejs wget postgresql-client \
	ca-certificates gsfonts cmake libnss-wrapper patch \
    && echo "# Install Diaspora" \
    && wget -q -O /usr/src/diaspora.tar.gz \
	https://github.com/diaspora/diaspora/archive/v$DIASPORA_VERS.tar.gz \
    && tar -xzf /usr/src/diaspora.tar.gz --strip 1 -C /diaspora \
    && mv /diaspora/config/database.yml.example /diaspora/config/database.yml \
    && ( \
	cd /diaspora \
	&& gem install bundler \
	&& ./script/configure_bundler \
	&& bundle install --full-index -j$(getconf _NPROCESSORS_ONLN) \
    ) \
    && mv /usr/src/diaspora.tar.gz /diaspora/public/source.tar.gz \
    && mv /reset-tls.sh /nsswrapper.sh /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && chown -R 1001:root /diaspora /etc/ssl /usr/local/share/ca-certificates \
    && chmod -R g=u /diaspora /etc/ssl /usr/local/share/ca-certificates \
    && echo "# Cleaning Up" \
    && apt-get remove --purge -y git wget build-essential libssl-dev libpq-dev \
	libcurl4-openssl-dev libxml2-dev libxslt-dev libmagickwand-dev cmake \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/diaspora/config/database.yml \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR /diaspora
ENTRYPOINT ["dumb-init","--","/run-diaspora.sh"]
CMD ["./script/server"]
