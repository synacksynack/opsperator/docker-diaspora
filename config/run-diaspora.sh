#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=true
else
    DO_DEBUG=false
fi

. /usr/local/bin/nsswrapper.sh

ADMIN_USER=${ADMIN_USER:-podr00t}
COUNTRY=${COUNTRY:-fr}
REDIS_HOST=${REDIS_HOST:-127.0.0.1}
REDIS_PORT=${REDIS_PORT:-6379}
SITE_FQDN=${SITE_FQDN:-diaspora.demo.local}
SITE_NAME=${SITE_NAME:-KubeSocial}
SK_WORKERS=${SK_WORKERS:-5}
UC_WORKERS=${SK_WORKERS:-2}

if test "$CAMO_HOST" -a "$CAMO_KEY"; then
    DO_CAMO=true
    CAMO_PATH=${CAMO_PATH:-/}
else
    DO_CAMO=false
    CAMO_HOST=example.com
    CAMO_KEY=secret
    CAMO_PATH=/camo
fi
if test "$PIWIK_HOST"; then
    DO_PIWIK=true
    PIWIK_SITE_ID=${PIWIK_SITE_ID:-1}
else
    DO_PIWIK=false
    PIWIK_HOST=piwik.example.com
    PIWIK_SITE_ID=1
fi
if test -z "$SMTP_HELO"; then
    SMTP_HELO=$SITE_FQDN
fi
if test "$SMTP_HOST"; then
    DO_SMTP=true
    SMTP_PORT=${SMTP_PORT:-25}
    SMTP_STARTTLS=${SMTP_STARTTLS:-false}
    SMTP_VERIFY=${SMTP_VERIFY:-none}
    if ! test "$SMTP_VERIFY" = none; then
	SMTP_VERIFY=peer
    fi
else
    DO_SMTP=false
    SMTP_HOST=smtp.example.com
    SMTP_PORT=25
    SMTP_STARTTLS=false
    SMTP_VERIFY=none
fi
if test -z "$ADMIN_CONTACT"; then
    ADMIN_CONTACT=$ADMIN_USER@$SITE_FQDN
fi

cpt=0
if test "$MYSQL_HOST"; then
    DB_ADAPTER=mysql
    DB_HOST=$MYSQL_HOST
    DB_NAME=${MYSQL_DB:-diaspora}
    DB_PASS=${MYSQL_PASS:-secret}
    DB_PORT=${MYSQL_PORT:-3306}
    DB_USER=${MYSQL_USER:-diaspora}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$DB_USER" \
		--password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
else
    DB_ADAPTER=postgresql
    DB_HOST=${POSTGRES_HOST:-127.0.0.1}
    DB_NAME=${POSTGRES_DB:-diaspora}
    DB_PASS=${POSTGRES_PASS:-secret}
    DB_PORT=${POSTGRES_PORT:-5432}
    DB_USER=${POSTGRES_USER:-diaspora}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$DB_PASS" \
		psql -U "$DB_USER" -h "$DB_HOST" \
		-p "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
fi

export HOME=`pwd`
sed -e "s|DB_HOST|$DB_HOST|" \
    -e "s|DB_NAME|$DB_NAME|" \
    -e "s|DB_USER|$DB_USER|" \
    -e "s|DB_PASS|$DB_PASS|" \
    -e "s|DB_PORT|$DB_PORT|" \
    -e "s|DB_ADAPTER|$DB_ADAPTER|" \
    /database.tpl >$HOME/config/database.yml

sed -e "s|ADMIN_CONTACT|$ADMIN_CONTACT|" \
    -e "s|ADMIN_USER|$ADMIN_USER|" \
    -e "s|CAMO_HOST|$CAMO_HOST|" \
    -e "s|CAMO_KEY|$CAMO_KEY|" \
    -e "s|CAMO_PATH|$CAMO_PATH|" \
    -e "s|COUNTRY|$COUNTRY|" \
    -e "s|DO_CAMO|$DO_CAMO|" \
    -e "s|DO_DEBUG|$DO_DEBUG|" \
    -e "s|DO_PIWIK|$DO_PIWIK|" \
    -e "s|DO_SMTP|$DO_SMTP|" \
    -e "s|PIWIK_HOST|$PIWIK_HOST|" \
    -e "s|PIWIK_SITE_ID|$PIWIK_SITE_ID|" \
    -e "s|REDIS_HOST|$REDIS_HOST|" \
    -e "s|REDIS_PORT|$REDIS_PORT|" \
    -e "s|SITE_FQDN|$SITE_FQDN|" \
    -e "s|SITE_NAME|$SITE_NAME|" \
    -e "s|SK_WORKERS|$SK_WORKERS|" \
    -e "s|SMTP_HELO|$SMTP_HELO|" \
    -e "s|SMTP_HOST|$SMTP_HOST|" \
    -e "s|SMTP_PORT|$SMTP_PORT|" \
    -e "s|SMTP_STARTTLS|$SMTP_STARTTLS|" \
    -e "s|SMTP_VERIFY|$SMTP_VERIFY|" \
    -e "s|UC_WORKERS|$UC_WORKERS|" \
    /config.tpl >$HOME/config/diaspora.yml

build_assets()
{
    found=$(find public/assets/ -maxdepth 1 -name 'main-*.js' -print -quit 2>/dev/null)
    if test -z "$found"; then
	rake assets:precompile
    fi
}

case "$COMPONENT" in
    init)
	exec rake db:migrate
	;;
    sidekiq)
	/usr/local/bin/reset-tls.sh
	exec bundle exec sidekiq
	;;
    debug)
	sleep 86400
	;;
    unicorn)
	/usr/local/bin/reset-tls.sh
	build_assets
	rake db:migrate
	exec bundle exec unicorn -c ./config/unicorn.rb -E production
	;;
    *)
	/usr/local/bin/reset-tls.sh
	build_assets
	rake db:migrate
	exec $@
esac
