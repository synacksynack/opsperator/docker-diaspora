configuration: ## Section
  environment: ## Section
    url: "https://SITE_FQDN/"
    certificate_authorities: '/etc/ssl/certs/ca-certificates.crt'
    redis: 'redis://REDIS_HOST:REDIS_PORT'
    require_ssl: true
    single_process_mode: false

    sidekiq: ## Section
      concurrency: SK_WORKERS
      retry: 10
      backtrace: 15
      dead_jobs_limit: 1000
      dead_jobs_timeout: 15552000
      log: "log/sidekiq.log"

    s3: ## Section
      #enable: true
      key: 'S3_ACCESS_KEY'
      secret: 'S3_SECRET_KEY'
      bucket: 'S3_BUCKET'
      region: 'S3_REGION'
      cache : true
    #image_redirect_url: 'https://images.example.org'

    assets: ## Section
      serve: true

    logging: ## Section
      logrotate: ## Section
        enable: false

      debug: ## Section
        sql: DO_DEBUG
        federation: DO_DEBUG

  server: ## Section
    listen: '0.0.0.0:3000'
    rails_environment: 'production'
    stderr_log: 'log/unicorn-stderr.log'
    stdout_log: 'log/unicorn-stdout.log'
    unicorn_worker: UC_WORKERS
    unicorn_timeout: 90
    #embed_sidekiq_worker: false
    #sidekiq_workers: 1

  chat: ## Section
    #enabled: true
    #https://wiki.diasporafoundation.org/Integration/Chat#Installation.2FUpdate
    #https://wiki.diasporafoundation.org/Integration/Chat#Configuration
    #https://wiki.diasporafoundation.org/Integration/XMPP/Prosody

    server: ## Section
      #enabled: false
      #certs: 'config/certs'
      #https://wiki.diasporafoundation.org/Integration/Chat#Apache2
      #https://wiki.diasporafoundation.org/Integration/Chat#Nginx

      bosh: ## Section
        #proxy: true
        #proto: http
        #address: '0.0.0.0'
        #port: 5280
        #bind: '/http-bind'

      log: ## Section
        info: 'log/prosody.log'
        error: 'log/prosody.err'
        debug: DO_DEBUG

  map: ##Section
    mapbox:
      enabled: false
      #access_token: "youraccesstoken"
      #style: "mapbox/streets-v9"

  privacy: ## Section
    jquery_cdn: false

    piwik: ## Section
      enable: DO_PIWIK
      host: 'PIWIK_HOST'
      site_id: PIWIK_SITE_ID

    statistics: ## Section
      user_counts: true
      post_counts: true
      comment_counts: true

    camo: ## Section
      proxy_markdown_images: DO_CAMO
      proxy_opengraph_thumbnails: DO_CAMO
      proxy_remote_pod_images: DO_CAMO
      root: "https://CAMO_HOSTCAMO_PATH"
      key: "CAMO_KEY"

  settings: ## Section
    autofollow_on_join: true
    #bitcoin_address: "change_me"
    enable_registrations: true
    export_concurrency: 1
    #liberapay_username: "change_me"
    pod_name: "SITE_NAME"
    typhoeus_concurrency: 20
    typhoeus_verbose: false

    welcome_message: ##Section
      #enabled: false
      #subject: "Welcome Message"
      #text: "Hello %{username}, welcome to diaspora."

    invitations: ## Section
      open: true
      count: 25

    paypal_donations: ## Section
      enable: false

    community_spotlight: ## Section
      enable: false

    captcha: ## Section
      enable: true
      image_size: '120x20'
      captcha_length: 5
      image_style: 'random'
      distortion: 'medium'

    terms: ## Section
      enable: true
      jurisdiction: "COUNTRY"
      minimum_age: 18

    maintenance: ## Section
      remove_old_users: ## Section
        enable: false

    #source_url: 'https://github.com/diaspora/diaspora'
    changelog_url: 'https://github.com/diaspora/diaspora/blob/master/Changelog.md'
    default_color_theme: "original"

    default_metas:
      title: 'SITE_NAME social network'
      description: 'SITE_NAME is the online social world where you are in control.'

    csp:
      report_only: true

  services: ## Section
    twitter: ## Section
      enable: false

    tumblr: ## Section
      enable: false

    wordpress: ## Section
      enable: false

  mail: ## Section
    enable: DO_SMTP
    sender_address: 'no-reply@SITE_FQDN'
    method: 'smtp'

    smtp: ## Section
      host: 'SMTP_HOST'
      port: SMTP_PORT
      authentication: 'none'
      starttls_auto: SMTP_STARTTLS
      domain: 'SMTP_HELO'
      openssl_verify_mode: 'SMTP_VERIFY'

    sendmail: ## Section
      location: '/usr/sbin/sendmail'

  admins: ## Section
    account: 'ADMIN_USER'
    podmin_email: 'ADMIN_CONTACT'

  relay: ## Section
    outbound: ## Section
      send: false

    inbound: ## Section
      subscribe: false

production: ## Section
  environment: ## Section
    #nope

development: ## Section
  environment: ## Section
    #nope
