#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/diaspora-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to diaspora
	(
	    cat /etc/group
	    echo "diaspora:x:`id -g`:"
	) >/tmp/diaspora-group
	(
	    cat /etc/passwd
	    echo "diaspora:x:`id -u`:`id -g`:diaspora:/diaspora:/bin/sh"
	) >/tmp/diaspora-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/diaspora-passwd
    export NSS_WRAPPER_GROUP=/tmp/diaspora-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
