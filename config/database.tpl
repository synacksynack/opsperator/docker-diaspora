postgresql: &postgresql
  adapter: postgresql
  host: "DB_HOST"
  port: DB_PORT
  username: "DB_USER"
  password: "DB_PASS"
  encoding: unicode

mysql: &mysql
  adapter: mysql2
  host: "DB_HOST"
  port: DB_PORT
  username: "DB_USER"
  password: "DB_PASS"
  encoding: utf8mb4
  collation: utf8mb4_bin

common: &common
  <<: *DB_ADAPTER

  # Should match environment.sidekiq.concurrency
  #pool: 25

combined: &combined
  <<: *common
development:
  <<: *combined
  database: DB_NAME
production:
  <<: *combined
  database: DB_NAME
test:
  <<: *combined
  database: DB_NAME
integration1:
  <<: *combined
  database: DB_NAME
integration2:
  <<: *combined
  database: DB_NAME
