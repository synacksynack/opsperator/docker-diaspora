---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: diaspora-kube
  name: diaspora-kube
  namespace: ci
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      name: diaspora-kube
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        name: diaspora-kube
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - diaspora-kube
            topologyKey: kubernetes.io/hostname
      containers:
      - env:
        - name: COUNTRY
          value: fr
        - name: DEBUG
        - name: HTTP_PROXY
        - name: HTTPS_PROXY
        - name: NO_PROXY
        - name: POSTGRES_DB
          valueFrom:
            secretKeyRef:
              key: database-name
              name: diaspora-postgres-kube
        - name: POSTGRES_HOST
          value: diaspora-postgres-kube
        - name: POSTGRES_PASS
          valueFrom:
            secretKeyRef:
              key: database-password
              name: diaspora-postgres-kube
        - name: POSTGRES_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: diaspora-postgres-kube
        - name: REDIS_HOST
          value: diaspora-redis-kube
        - name: SITE_FQDN
          value: diaspora.ci.apps.intra.unetresgrossebite.com
        - name: SITE_NAME
          value: KubeSocial
        - name: SK_WORKERS
          value: "5"
        - name: SMTP_HELO
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: spec.nodeName
        - name: SMTP_HOST
          value: postfix-kube.ci.svc.cluster.local
        - name: SMTP_PORT
          value: "465"
        - name: SMTP_PROTO
          value: smtps
        - name: SMTP_STARTTLS
          value: "false"
        - name: TZ
          value: Europe/Paris
        - name: UC_WORKERS
          value: "2"
        image: registry.gitlab.com/synacksynack/opsperator/docker-diaspora:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 118
          httpGet:
            httpHeaders:
            - name: Host
              value: diaspora.ci.apps.intra.unetresgrossebite.com
            - name: X-Forwarded-Proto
              value: https
            path: /podmin
            port: 3000
            scheme: HTTP
          initialDelaySeconds: 120
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        name: diaspora
        ports:
        - containerPort: 3000
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            httpHeaders:
            - name: Host
              value: diaspora.ci.apps.intra.unetresgrossebite.com
            - name: X-Forwarded-Proto
              value: https
            path: /podmin
            port: 3000
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            memory: 1Gi
          requests:
            cpu: 50m
            memory: 512Mi
        volumeMounts:
        - mountPath: /diaspora/public/uploads
          name: data
      volumes:
      - emptyDir: {}
        name: data
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: diaspora-postgres-kube
  name: diaspora-postgres-kube
  namespace: ci
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      name: diaspora-postgres-kube
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        name: diaspora-postgres-kube
    spec:
      containers:
      - env:
        - name: DEBUG
        - name: POSTGRESQL_ADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-admin-password
              name: diaspora-postgres-kube
        - name: POSTGRESQL_DATABASE
          valueFrom:
            secretKeyRef:
              key: database-name
              name: diaspora-postgres-kube
        - name: POSTGRESQL_MAX_CONNECTIONS
          value: "100"
        - name: POSTGRESQL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-password
              name: diaspora-postgres-kube
        - name: POSTGRESQL_SHARED_BUFFERS
          value: 12MB
        - name: POSTGRESQL_UPGRADE
        - name: POSTGRESQL_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: diaspora-postgres-kube
        - name: POSTGRESQL_VERSION
          value: "12"
        - name: TZ
          value: Europe/Paris
        image: docker.io/centos/postgresql-12-centos8:latest
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - /bin/sh
            - -i
            - -c
            - pg_isready -h 127.0.0.1 -p 5432
          failureThreshold: 38
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          timeoutSeconds: 3
        name: postgres
        ports:
        - containerPort: 5432
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - /bin/sh
            - -i
            - -c
            - psql -h 127.0.0.1 -U $POSTGRESQL_USER -q -d $POSTGRESQL_DATABASE -c
              'SELECT 1'
          failureThreshold: 3
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 250m
            memory: 512Mi
          requests:
            cpu: 250m
            memory: 512Mi
        volumeMounts:
        - mountPath: /var/lib/pgsql/data
          name: db
      - env:
        - name: DATA_SOURCE_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: diaspora-postgres-kube
        - name: DATA_SOURCE_PASS
          valueFrom:
            secretKeyRef:
              key: database-password
              name: diaspora-postgres-kube
        - name: DATA_SOURCE_HOST
          value: 127.0.0.1
        - name: DATA_SOURCE_DBNAME
          valueFrom:
            secretKeyRef:
              key: database-name
              name: diaspora-postgres-kube
        - name: DEBUG
        image: registry.gitlab.com/synacksynack/opsperator/docker-pgexporter:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 38
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          tcpSocket:
            port: 9113
          timeoutSeconds: 3
        name: exporter
        ports:
        - containerPort: 9113
          protocol: TCP
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 100m
            memory: 128Mi
      securityContext:
        runAsUser: 26
      terminationGracePeriodSeconds: 600
      volumes:
      - emptyDir: {}
        name: db
