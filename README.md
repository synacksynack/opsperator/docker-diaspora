# k8s Diaspora

Diaspora image.

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                      | Default                                                     |
| :----------------------------- | ----------------------------------- | ----------------------------------------------------------- |
|  `ADMIN_CONTACT`               | Diaspora Admin Contact              | `$ADMIN_USER`@`$SITE_FQDN`                                  |
|  `ADMIN_USER`                  | Diaspora Admin User                 | `podr00t`                                                   |
|  `CAMO_HOST`                   | Diaspora Camo Host                  | undef                                                       |
|  `CAMO_KEY`                    | Diaspora Camo Key                   | undef                                                       |
|  `CAMO_PATH`                   | Diaspora Camo Path                  | `/`                                                         |
|  `COMPONENT`                   | Diaspora Worker to Start            | undef (starts all, otherwise `sidekiq or `unicorn`)         |
|  `COUNTRY`                     | Diaspora Country Code               | `fr`                                                        |
|  `MYSQL_DB`                    | Diaspora MySQL Database             | `diaspora`                                                  |
|  `MYSQL_HOST`                  | Diaspora MySQL Host                 | undef, set one to enable MySQL, otherwise Postgres used     |
|  `MYSQL_PASS`                  | Diaspora MySQL Password             | `secret`                                                    |
|  `MYSQL_PORT`                  | Diaspora MySQL Port                 | `5432`                                                      |
|  `MYSQL_USER`                  | Diaspora MySQL Username             | `diaspora`                                                  |
|  `ONLY_TRUST_KUBE_CA`          | Don't trust base image CAs          | `false`, any other value disables ca-certificates CAs       |
|  `PIWIK_SITE_ID`               | Matomo Site ID                      | `1`                                                         |
|  `PIWIK_HOST`                  | Matomo Server FQDN                  | undef                                                       |
|  `POSTGRES_DB`                 | Diaspora Postgres Database          | `diaspora`                                                  |
|  `POSTGRES_HOST`               | Diaspora Postgres Host              | `127.0.0.1`                                                 |
|  `POSTGRES_PASS`               | Diaspora Postgres Password          | `secret`                                                    |
|  `POSTGRES_PORT`               | Diaspora Postgres Port              | `5432`                                                      |
|  `POSTGRES_USER`               | Diaspora Postgres Username          | `diaspora`                                                  |
|  `REDIS_HOST`                  | Diaspora Redis Host                 | `127.0.0.1`                                                 |
|  `REDIS_PORT`                  | Diaspora Redis Port                 | `6379`                                                      |
|  `SITE_FQDN`                   | Diaspora Site FQDN                  | `diaspora.demo.local`                                       |
|  `SITE_NAME`                   | Diaspora Site Name                  | `KubeSocial`                                                |
|  `SK_WORKERS`                  | Diaspora Sidekiq Workers Count      | `5`                                                         |
|  `SMTP_HELO`                   | Diaspora SMTP HELO FQDN             | undef                                                       |
|  `SMTP_HOST`                   | Diaspora SMTP Relay                 | undef                                                       |
|  `SMTP_PORT`                   | Diaspora SMTP Port                  | `25`                                                        |
|  `SMTP_STARTTLS`               | Diaspora SMTP uses StartTLS         | `false`                                                     |
|  `SMTP_VERIFY`                 | Diaspora SMTP TLS Verify            | `none`                                                      |
|  `UC_WORKERS`                  | Diaspora Unicorn Workers Count      | `2`                                                         |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point         | Description                                 |
| :-------------------------- | ------------------------------------------- |
|  `/certs`                   | Diaspora Certificate Authorities (optional) |
|  `/diaspora/public/uploads` | Diaspora Uploads directory                  |
